package com.epam.comparator;

import com.epam.model.Gem;

import java.util.Comparator;

public class NameComparator implements Comparator<Gem> {
    @Override
    public int compare(Gem gem1, Gem gem2) {
        return gem1.getName().compareTo(gem2.getName());
    }
}

