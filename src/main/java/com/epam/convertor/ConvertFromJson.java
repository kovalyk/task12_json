package com.epam.convertor;

import com.epam.comparator.NameComparator;
import com.epam.model.Gem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collections;
import java.util.List;
public class ConvertFromJson {

        private static final Gson GSON = new GsonBuilder().create();
        private static final String JSON_FILE = "gems.json";
        private static List<Gem> gems;

        public static void main(String[] args) {
            try {
                JsonObject jsonObject = GSON.fromJson(new FileReader(JSON_FILE), JsonObject.class);
                JsonArray jsonElements = jsonObject.getAsJsonArray("gems");
                gems = GSON.fromJson(jsonElements, new TypeToken<List<Gem>>(){}.getType());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        Collections.sort(gems, new NameComparator());
            for (Gem gem : gems) {
                System.out.println(gem);
            }
        }
}
