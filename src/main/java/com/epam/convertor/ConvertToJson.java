package com.epam.convertor;

import com.epam.model.Gem;
import com.epam.model.GemCollection;
import com.epam.model.VisualParameter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;

public class ConvertToJson {

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private static final String FILENAME = "gems.json";

    public static void main(String[] args) {
        VisualParameter visualParameter = new VisualParameter("blue", 100, 15);
        Gem gem = new Gem(0, "alexandrite", "precious",
                "Ghana", visualParameter, 1);
        VisualParameter visualParameter1 = new VisualParameter("violet", 100, 9);
        Gem gem1 = new Gem(1, "amethyst", "semi precious",
                "Germany", visualParameter1, 5);
        VisualParameter visualParameter2 = new VisualParameter("dark blue", 60, 15);
        Gem gem2 = new Gem(2, "sapphire", "precious",
                "India", visualParameter2, 2);
        VisualParameter visualParameter3 = new VisualParameter("red", 50, 11);
        Gem gem3 = new Gem(3, "ruby", "precious",
                "East Africa", visualParameter3, 4);
        VisualParameter visualParameter4 = new VisualParameter("green", 90, 13);
        Gem gem4 = new Gem(4, "emerald", "precious",
                "Colombia", visualParameter4, 6);
        VisualParameter visualParameter5 = new VisualParameter("cherry red", 78, 10);
        Gem gem5 = new Gem(5, "garnet", "semi precious",
                "Brazil", visualParameter5, 5);

       GemCollection jsonGems = new GemCollection();
        jsonGems.addGem(gem);
        jsonGems.addGem(gem1);
        jsonGems.addGem(gem2);
        jsonGems.addGem(gem3);
        jsonGems.addGem(gem4);
        jsonGems.addGem(gem5);

        String json = GSON.toJson(jsonGems);
        System.out.println(json);

        try (FileWriter writer = new FileWriter(FILENAME)) {
            writer.write(json);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
