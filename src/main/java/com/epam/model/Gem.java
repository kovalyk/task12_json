package com.epam.model;

public class Gem {
    private int gemNumber;
    private String name;
    private String preciousness;
    private String origin;
    private VisualParameter visualParameter;
    private double value;

    public Gem() {
    }

    public Gem(Integer gemNumber, String name, String preciousness, String origin, VisualParameter visualParameter, double value) {
        this.gemNumber = gemNumber;
        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.visualParameter = visualParameter;
        this.value = value;
    }

    public int getGemNumber() {
        return gemNumber;
    }

    public void setGemNumber(int gemNumber) {
        this.gemNumber = gemNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(String preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameter getVisualParameter() {
        return visualParameter;
    }

    public void setVisualParameter(VisualParameter visualParameter) {
        this.visualParameter = visualParameter;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gem gem = (Gem) o;

        if (gemNumber != gem.gemNumber) return false;
        return name.equals(gem.name);
    }

    @Override
    public int hashCode() {
        int result = gemNumber;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Gem{" +
                "gemNumber=" + gemNumber +
                ", name='" + name + '\'' +
                ", preciousness='" + preciousness + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameter=" + visualParameter +
                ", value=" + value +
                '}';
    }
}

