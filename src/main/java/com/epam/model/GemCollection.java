package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public class GemCollection {

    private List<Gem> gems;

    public GemCollection() {
        gems = new ArrayList<Gem>();
    }

    public List<Gem> getGems() {
        return gems;
    }

    public void setGems(List<Gem> gems) {
        this.gems = gems;
    }

    public void addGem(Gem gem) {
        gems.add(gem);
    }
}
