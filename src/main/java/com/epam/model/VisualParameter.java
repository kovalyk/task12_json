package com.epam.model;

public class VisualParameter {

    private String color;
    private int transparency;
    private int facetsNumber;

    public VisualParameter() {
    }

    public VisualParameter(String color, int transparency, int facetsNumber) {
        this.color = color;
        this.transparency = transparency;
        this.facetsNumber = facetsNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }

    public int getFacetsNumber() {
        return facetsNumber;
    }

    public void setFacetsNumber(int facetsNumber) {
        this.facetsNumber = facetsNumber;
    }

    @Override
    public String toString() {
        return "color: " + color + ", transparency: " + transparency +
                "%, facetsNumber: " + facetsNumber + " facets";
    }
}
